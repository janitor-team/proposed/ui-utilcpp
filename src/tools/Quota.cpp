// Local
#include "config.h"

// STDC++
#include <iostream>
#include <string>

// POSIX C

// C++ Libraries
#include <ui-utilcpp/File.hpp>
#include <ui-utilcpp/QuotaInfo.hpp>
#include <ui-utilcpp/Time.hpp>

int main(int argc, char *argv[])
{
	std::string arg1(""); if (argc > 1) arg1=argv[1];
	std::string arg2(""); if (argc > 2) arg2=argv[2];

	int exitCode(0);

	try
	{
		UI::Util::FsInfo const fsInfo("", arg1);
		std::cout << "FsInfo(" << arg1 << "): [1024er blocks]" << std::endl;
		std::cout << "  Total: " << fsInfo.getTotal() << std::endl;
		std::cout << "  Avail: " << fsInfo.getAvail() << std::endl;
		std::cout << "  Free: " << fsInfo.getFree() << std::endl;
		std::cout << "  Used: " << fsInfo.getUsed() << std::endl << std::endl;

		UI::Util::QuotaInfo::FS const fs(UI::Util::QuotaInfo::file2fs(arg1));
		std::cout << "All methods: " << fs.getMethods("__all__") << std::endl;
		std::cout << "Device/type: " << fs.getDevice() << "/" << fs.getType() << std::endl;
		std::cout << "FS  methods: " << fs.getMethods() << std::endl;

		UI::Util::QuotaInfo qi(fs, std::atoi(arg2.c_str()));

		std::cout << "Quota for uid: " << arg2.c_str() << std::endl;
		std::cout << "Method used  : " << qi.getMethod() << std::endl;
		std::cout << "Blocks : " << qi.getBlocks() << std::endl;
		std::cout << "BlockHL: " << qi.getBlockHL() << std::endl;
		std::cout << "BlockSL: " << qi.getBlockSL() << std::endl;
		std::cout << "BlockTL: " << qi.getBlockTL() << std::endl;
		std::cout << "Inodes : " << qi.getINodes() << std::endl;
		std::cout << "INodeHL: " << qi.getINodeHL() << std::endl;
		std::cout << "INodeSL: " << qi.getINodeSL() << std::endl;
		std::cout << "INodeTL: " << qi.getINodeTL() << std::endl;
	}
	catch (UI::Exception const & e)
	{
		std::cerr << e.what() << std::endl << std::endl;
		std::cerr << "Usage: quota <ANY_FILE_ON_FS> <UID>" << std::endl;
		exitCode=1;
	}

	return exitCode;
}
