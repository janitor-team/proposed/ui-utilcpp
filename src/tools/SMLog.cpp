// Local configuration
#include "config.h"

// C++ Libraries
#include <ui-utilcpp/SMLog.hpp>

int main()
{
	// configure
	ost::slog.open("ui-utilcpp_smlogm", ost::Slog::classUser);
	ost::slog.clogEnable(true);

	SM_LOGEMERG("EMERG log");
	SM_LOGALERT("ALERT log");
	SM_LOGCRIT("CRIT log");
	SM_LOGERR("ERR log");
	SM_LOGWARNING("WARNING log");
	SM_LOGNOTICE("NOTICE log");
	SM_LOGINFO("INFO log");
	SM_LOGDEBUG("DEBUG log");
}
