// Local configuration
#include "config.h"

// C++ Libraries
#include <ui-utilcpp/SMLogMono.hpp>

int main()
{
	// configure syslog
	UI::Util::SysLogMonoSingleton logger("ui-utilcpp_smlogmono", LOG_PERROR, LOG_USER);

	SM_LOGEMERG("EMERG log");
	SM_LOGALERT("ALERT log");
	SM_LOGCRIT("CRIT log");
	SM_LOGERR("ERR log");
	SM_LOGWARNING("WARNING log");
	SM_LOGNOTICE("NOTICE log");
	SM_LOGINFO("INFO log");
	SM_LOGDEBUG("DEBUG log");
}
