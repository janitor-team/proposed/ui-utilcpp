// Local
#include "config.h"

// STDC++
#include <iostream>
#include <fstream>
#include <vector>

// POSIX C

// C++ Libraries
#include <ui-utilcpp/CmdLine.hpp>

class TestCmd: public UI::Util::CmdLine::Cmd
{
public:
	TestCmd()
		:Cmd("test", "Test command")
	{
		addArg("testarg",  "Mandatory test argument documentation");
		addOptArg("testoptarg",  "Optional test argument documentation");
	}
private:
	int runCmd()
	{
		cl_->os() << "Test command running" << std::endl;
		cl_->os() << "Arg(1) == " << getArg(1) << std::endl;
		cl_->os() << "Arg(2) == " << getArg(2) << std::endl;

		// Return "0" for ok, anything else for error
		return(0);
	}
};

class TestCmdLine: public UI::Util::CmdLine::CmdLine
{
public:
	TestCmdLine(std::istream * is, std::ostream * os)
	:CmdLine(is, os, &std::cerr, "Test Command Line", "\nTest Prompt# ")
	{
		// You can set some intital variables here
		setVar("NATIVE_MOJO", "YES");

		// You add any amount of Commands here
		add(new UI::Util::CmdLine::HeaderCmd("Custom command line functions"));
		add(new TestCmd());
	}

	~TestCmdLine()
	{}
};

int main()
{
	return(TestCmdLine(0, &std::cout).run());
}
