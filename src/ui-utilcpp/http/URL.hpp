/**
 * @file
 */
#ifndef UI_UTIL_HTTP_URL_HPP
#define UI_UTIL_HTTP_URL_HPP

// STDC++
#include <string>

// C++ Libraries
#include <ui-utilcpp/http/Connection.hpp>

namespace UI {
namespace Util {
namespace Http {

/** @brief HTTP URL parser. @see RFC 2616, 3.2.2. */
class URL
{
private:
	std::string const url_;
	std::string       host_;
	unsigned int      port_;
	std::string       path_;

public:
	URL(std::string const & url);
	/** @brief Add param to query part (auto-adds query part if not already added) */
	URL & addParam(std::string const & key, std::string const & value);

	/** @name Get original url string, host, port and path.
	 *
	 * @note Path includes the optional "query" and is prepared to be
	 * used as request URI.
	 *
	 * @{ */
	std::string  const & getHost() const;
	unsigned int const & getPort() const;
	std::string  const & getPath() const;
	std::string  const   getURL()  const;
	/** @} */

};

class URLGet
{
private:
	URL const   url_;
	StatusLine  status_;
	Header      header_;
	std::string body_;

public:
	URLGet(std::string const & url, long int const & timeout=0);

	URL         const & getURL()    const;
	StatusLine  const & getStatus() const;
	Header      const & getHeader() const;
	std::string const & getBody()   const;
};

}}}
#endif
