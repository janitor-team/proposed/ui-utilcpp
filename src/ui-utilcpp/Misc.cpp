#include "config.h"

// Implementation
#include "Misc.hpp"

// Local
#include "Text.hpp"

namespace UI {
namespace Util {

std::string realpath(std::string const & path)
{
	char buffer[PATH_MAX];
	Sys::realpath(path.c_str(), buffer);
	return std::string(buffer);
}

EUIDSwap::EUIDSwap(uid_t uid)
	:origEUID_(Sys::geteuid())
{
	try { Sys::seteuid(uid); }
	catch(std::exception const & e) { UI_THROW_CODE(SetEuidErr_, "Cannot set euid to " + tos(uid) + ": " + e.what()); }
}

EUIDSwap::~EUIDSwap()
{
	try { Sys::seteuid(origEUID_); }
	catch(...) { }
}

uid_t EUIDSwap::getOrigUID() const
{
	return origEUID_;
}

}}
